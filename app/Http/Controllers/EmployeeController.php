<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeController extends Controller
{
    /**
     * employee listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Employee::latest()->get();

            if ($request->filled('from_date') && $request->filled('to_date')) {
                $fromDate = date('Y-m-d', strtotime($request->from_date));
                $toDate = date('Y-m-d', strtotime($request->to_date));
                $data = $data->whereBetween('joining_date', [$fromDate, $toDate]);
            }

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('profile_image', function ($row) {
                    return '<img src="' . $row->profile_image . '" border="0"
                        width="40" class="img-rounded" align="center" />';
                })
                ->addColumn('employee_name', function ($row) {
                    return ucfirst($row->first_name) . ' ' . ucfirst($row->last_name);
                })
                ->addColumn('joining_date', function ($row) {
                    return date('d F, Y', strtotime($row->joining_date));
                })
                ->rawColumns(['profile_image'])
                ->make(true);
        }

        return view('employees.index');
    }

    /**
     * Ceate employee profile.
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store store employee profile.
     */
    public function store(Request $request)
    {
        request()->validate([
            'profile_image'  => 'required|image|max:2048',
            'first_name'  => 'required|string|max:100',
            'last_name'  => 'required|string|max:100',
            'joining_date'  => 'required|date',
        ]);

        if ($files = $request->file('profile_image')) {

            $file = $request->file('profile_image');
            $destinationPath = 'employee/img/';
            $originalFile = time() . '.' . $request->profile_image->extension();
            $fileStore = $file->move($destinationPath, $originalFile);

            $employee = new Employee();
            $employee->first_name = $request->first_name;
            $employee->last_name = $request->last_name;
            $employee->joining_date = date('Y-m-d', strtotime($request->joining_date));
            $employee->profile_image = $fileStore;
            $employee->save();

            $employeeId = $employee->id;
            $employeeCode = 'EMP-' . str_pad($employeeId, 4, '0', STR_PAD_LEFT);
            $employee->update(['employee_code' => $employeeCode]);
            return Response()->json([
                "success" => true,
                "file" => $file
            ]);
        }
    }
}
