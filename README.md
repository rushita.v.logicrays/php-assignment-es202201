# PHP Assignment - #ES202201

-   Assignment of Employee Profile.

# Technology

-   Laravel 10, PHP 8.2, Mysql

## Features

Create a form which accepts following fields and submit the same using Ajax.

● Employee Code (Auto Generate Series: EMP-0001)

● First Name

● Last Name

● Joining Date (Date-picker optional)

● Profile Image (max 2MB validation optional)

Store the data in any one of the following database

● mySQL

Show all the data entered in data-table having columns

● Employee Code

● Profile Image

● Full Name

● Joining Date

The data-table should have following features (supported using ajax only)

● Filter -> Date range to show list of people joined in that particular period

● Pagination support

## Installation

-   Clone this repository to your local.

-   Run below command in project directory.

```sh
composer install
```

Provide below db configuration in **.env** file

```sh
DB_HOST=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

Run below command for db schema migration:

```sh
php artisan migrate
```

Start local server

```sh
php artisan serve
```
