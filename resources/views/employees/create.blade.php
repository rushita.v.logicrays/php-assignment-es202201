@extends('layouts.master')

@section('content')
    <div class="py-4">
        <div class="d-flex justify-content-between w-100 flex-wrap">
            <div class="mb-3 mb-lg-0">
                <h1 class="h4">Create Employee Profile</h1>
            </div>
            <div>
                <a href="{{ route('employees') }}" class="btn btn-gray-800 d-inline-flex align-items-center">
                    <svg class="icon icon-xs me-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="1em"
                        viewBox="0 0 512 512">
                        <path
                            d="M9.4 233.4c-12.5 12.5-12.5 32.8 0 45.3l128 128c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L109.3 288 480 288c17.7 0 32-14.3 32-32s-14.3-32-32-32l-370.7 0 73.4-73.4c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-128 128z" />
                    </svg>
                    Employees List
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 mb-4">
            <div id="soccessAlert" class="alert alert-success" role="alert">
                Employee Profile saved successfully!
            </div>
            <div class="card border-0 shadow components-section">
                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" id="employeeForm" action="javascript:void(0)">
                        <div class="row mb-4">
                            <div class="col-lg-4 col-sm-6">
                                <!-- Form -->
                                <div class="mb-4">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name"
                                        aria-describedby="first_nameHelp" placeholder="Entre first name">
                                    <span id="first_name_error" class="text-danger validateErr">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <!-- Form -->
                                <div class="mb-4">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name"
                                        aria-describedby="last_namelHelp" placeholder="Enter last name">
                                    <span id="last_name_error" class="text-danger validateErr">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-4 col-sm-6">
                                <!-- Form -->
                                <div class="mb-3">
                                    <label for="joining_date">Date of Joining</label>
                                    <div class="input-group">
                                        <span class="input-group-text">
                                            <svg class="icon icon-xs text-gray-600" fill="currentColor" viewBox="0 0 20 20"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd"
                                                    d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                                    clip-rule="evenodd"></path>
                                            </svg>
                                        </span>
                                        <input data-datepicker="" class="form-control" name="joining_date" id="joining_date"
                                            type="text" placeholder="DD/MM/YYYY" autocomplete="off">
                                    </div>
                                    <span id="joining_date_error" class="text-danger validateErr">
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <!-- Form -->
                                <div class="mb-3">
                                    <label for="profile_image" class="form-label">Profile Image</label>
                                    <input class="form-control" type="file" name="profile_image" id="profile_image">
                                    <span id="profile_image_error" class="text-danger validateErr">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" type="button">Save</button>
                        <a href="{{ route('employees') }}" class="btn btn-danger d-inline-flex align-items-center">
                            Cancle
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(e) {
            $("#soccessAlert").hide();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#employeeForm').submit(function(e) {
                e.preventDefault();

                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: "{{ route('employee.store') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: (data) => {
                        this.reset();
                        $(".is-invalid").removeClass("is-invalid");
                        $(".validateErr").empty();
                        $("#soccessAlert").show();
                    },
                    error: function(data) {
                        if (data.status === 422) {
                            $(".is-invalid").removeClass("is-invalid");
                            $(".validateErr").empty();
                            var errorsRes = data.responseJSON.errors;
                            $.each(errorsRes, function(key, val) {
                                $("#" + key + "_error").text(val[0]);
                                $("#" + key).addClass("is-invalid");
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection
