@extends('layouts.master')

@section('content')
    <div class="py-4">
        <div class="d-flex justify-content-between w-100 flex-wrap">
            <div class="mb-3 mb-lg-0">
                <h1 class="h4">Employees List</h1>
            </div>
            <div>
                <a href="{{ route('employee.create') }}" class="btn btn-gray-800 d-inline-flex align-items-center">
                    <svg class="icon icon-xs me-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="1em"
                        viewBox="0 0 640 512">
                        <path
                            d="M96 128a128 128 0 1 1 256 0A128 128 0 1 1 96 128zM0 482.3C0 383.8 79.8 304 178.3 304h91.4C368.2 304 448 383.8 448 482.3c0 16.4-13.3 29.7-29.7 29.7H29.7C13.3 512 0 498.7 0 482.3zM504 312V248H440c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V136c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H552v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z" />
                    </svg>
                    Create Employee
                </a>
            </div>
        </div>
    </div>

    <div class="card border-0 shadow mb-1">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <!-- Form -->
                    <div class="mb-3">
                        <label for="from_date">From Date of Joining</label>
                        <div class="input-group">
                            <span class="input-group-text">
                                <svg class="icon icon-xs text-gray-600" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                        clip-rule="evenodd"></path>
                                </svg>
                            </span>
                            <input data-datepicker="" class="form-control" name="from_date" id="from_date" type="text"
                                placeholder="DD/MM/YYYY" autocomplete="off">
                        </div>
                        </span>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <!-- Form -->
                    <div class="mb-3">
                        <label for="to_date">To Date of Joining</label>
                        <div class="input-group">
                            <span class="input-group-text">
                                <svg class="icon icon-xs text-gray-600" fill="currentColor" viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                        d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z"
                                        clip-rule="evenodd"></path>
                                </svg>
                            </span>
                            <input data-datepicker="" class="form-control" name="to_date" id="to_date" type="text"
                                placeholder="DD/MM/YYYY" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 pt-4 mt-2">
                    <div class="mb-5">
                        <button type="button" id="filter" class="btn btn-primary" type="button">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card border-0 shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table id="employeeTable" class="table table-centered table-nowrap mb-0 rounded">
                    <thead class="thead-light">
                        <tr>
                            <th class="border-0 rounded-start">#</th>
                            <th class="border-0">Profile Image</th>
                            <th class="border-0">Employee Code</th>
                            <th class="border-0">Employee Name</th>
                            <th class="border-0 rounded-end">Joining Date</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(function() {

            var employeeTable = $('#employeeTable').DataTable({
                processing: true,
                serverSide: true,
                // ajax: "{{ route('employees') }}",
                ajax: {
                    url: "{{ route('employees') }}",
                    data: function(d) {
                        d.from_date = $('#from_date').val();
                        d.to_date = $('#to_date').val();
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'profile_image',
                        name: 'profile_image'
                    },
                    {
                        data: 'employee_code',
                        name: 'employee_code'
                    },
                    {
                        data: 'employee_name',
                        name: 'employee_name'
                    },
                    {
                        data: 'joining_date',
                        name: 'joining_date'
                    },
                ]
            });

            $("#filter").click(function() {
                employeeTable.draw();
            });

        });
    </script>
@endsection
